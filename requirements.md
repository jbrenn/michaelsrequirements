# python reconstruction pipeline



1.  Introduction

    1. Purposes

        *Reconstruction of tomography data for subsequent STA or segmentation*
        - as user-less as possible
            - one raw folder and one init file
            - automatic parameter estimation / parsing
        - data bookkeeping enforcement, what strain/condition
            - good "tomolist" alternative
            - Integration into all-project-spanning *MPIB-cryo database*
            - STOP assuming filenames, keep them stored somewhere
        - tailored to our cluster/hpcl architecture
        - integration of all our microscopes and acquisition software
            1. Tomo5
            2. SerialEM
        - realtime capabilities with watchdogs and event-based architecture
        - live insights in a "dashboard"
            - similar to warp?
        - CI/CD?
        - Maintaining downwards compatibility 
            - for tomoman/subtom?
            - for early pipeline versions?

        ``` 
        modularity is key for fast integration of newly published algorithms    
        maybe even a "specification of the interface" for other labs
        ```

        - __Steps__:
            1. Specification of neccessary parameters
            2. Cleanup of stacks
            3. Motioncorr2
            4. Dose-filtering
            5. coarse alignment (IMOD)
            6. fine alignment (AreTomo)
            7. CTF-correction & WBP (Warp/NovaCTF)

            Optional additions:

            8. CryoCARE ( / IsoNet)
            11. Membrain-seg / TomosegmemTV / pyseg / ...

    2. Document Conventions
    3. Intended audience and reading suggestions
    4. Definitions, acronyms and abbreviations
    5. Scope
    6. References

---
2. Overall description
    1. Product perspective
    2. Product functions
    3. Deliverables and timeline
    4. User classes and characteristics
    5. Operating environment
    6. Design and implementation constraints
        1. Logical Architecture of the project
        2. Installation procedure
        3. Modularity
        4. Design pattern used
        5. File structure, copyright and license
        6. Coding workflow and git
    7. Testing procedures and quality assurance
    8. Coding style and conventions
        PEP8 Flake 8
        ReadTheDocs? Wiki? GitlabWiki?
    9. User documentation
    10. Assumptions and dependencies

---
3. Interfaces
    1. User Interfaces
    2. Hardware Interfaces
    3. Software & API Interfaces
    4. Communications Interfaces


--- 
4. System Features

    1. System Feature 1

        1. Description and Priority

        2. Stimulus and response sequences

        3. Functional requirements

    2. System Feature 2

---
5. Non-functional requirements
    1. Performance
    2. Safety
    3. Security
    4. Reliability
    5. Portability
    6. Quality assurance and compliance


---
6. Other requirements


---
7. Sustainability
    1. Growth analysis
    2. maintainability
    3. distribution ana packing
    4. Bug, issue solving and support
    5. Brand and copyright protection
    6. Funding Plan and financial model



